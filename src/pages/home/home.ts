import { Component, ViewChild, ElementRef, Renderer} from '@angular/core';  
import { NavController, Content, NavParams } from 'ionic-angular';  
import { FacebookService } from './../../services/facebook.service';

import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [FacebookService ],
})

export class HomePage  {
  currentP: string;
  count: any;

  isSingle: boolean;
  isAll: boolean;
  isPreference: boolean;
  isSearching: boolean;

  feed= [];
  feedInit=[];
  feedTmp= [];

  showHover : boolean;
  hideHover: boolean;

  @ViewChild(Content) content: Content;
  start = 0;
  threshold = 10;
  slideHeaderPrevious = 0;
  ionScroll:any;
  showheader:boolean;
  hideheader:boolean;

  postsObservable: any;

  posts: any=[];
  searches: Observable<any[]>;

  limit : number ;
  since : number ;
  until : number ;
  current: number;

  constructor(private storage: Storage ,public navCtrl: NavController , public renderer: Renderer ,public myElement: ElementRef,public navParams: NavParams ,public facebookService: FacebookService) {

    this.hideheader=true;
    this.showheader=false;

    this.showHover = false;
    this.hideHover=true;
    this.isSearching=false;

    this.feed= [];
    this.feedInit=[];
    this.feedInit.push("278276678903271", "348133635312225" , "213505205698292");

    this.current= Math.round((new Date()).getTime() / 1000);
    this.since= this.current - 604800;
    this.until= this.current;

    this.limit= 96 ; 
    this.getData();
    this.isAll = true;
    this.currentP= "";
  }

  // --------------end of constructor---------------

  search(value:string ){
    this.isSearching=true;
    this.searches = this.facebookService
    .getSearched(value)
    .map(data => data.map(this.map));
  }

  map = (res) => {
    return {
      name: res.name,
      id: res.id,
      cover: res.cover
    };
  }

  closeS(){
    if( this.hideHover){
      this.isSearching=false;
    }
  }


  getData(){
      this.storage.get("data").then((data) =>{
      this.feed=data;
      if( this.feed.length >0){
      this.postsObservable = this.facebookService.getFeedItems( data , this.limit, this.since , this. until).subscribe(response => {
      this.posts = response;
      
      this.postsObservable.unsubscribe();
      })
      this.content.scrollToTop();
    }
      else{
        this.getInit()
          this.content.scrollToTop();
      }
    });
  }

  getInit(){
    this.feed=this.feedInit;
    this.postsObservable = this.facebookService.getFeedItems( this.feedInit , this.limit, this.since , this. until).subscribe(response => {
      this.posts = response;
      this.postsObservable.unsubscribe();
    })
  }

 clearData(val:string){
   this.feed = this.feed.filter( item => item !==val);
    this.storage.set("data", this.feed);
    this.loadAll();

     if( this.feed.length == 0){
       this.getInit();
     }

 }

 clearAllData(){
  this.feed = [];
  this.storage.set("data", this.feed);
  this.getInit();
}

 loadAll(){  
  this.isAll = true;
  this.isSingle = false;
  this.isPreference=false;

  this.since= this.current - 604800;
  this.until= this.current;

  this.postsObservable = this.facebookService.getFeedItems( this.feed , this.limit, this.since , this. until).subscribe(response => {
    this.posts = response;
    this.postsObservable.unsubscribe();
    })
    this.content.scrollToTop();
  }

loadPreference(feed){
  this.isPreference= true;
  this.isAll=false;
  this.isSingle=false;

  this.since= this.current - 604800;
  this.until= this.current;

 this.feedTmp= this.feed.filter( item => item !== "348133635312225" && item !== "278276678903271" &&  item !== "213505205698292");
  this.postsObservable = this.facebookService.getFeedItems( this.feedTmp , this.limit, this.since , this. until).subscribe(response => {
    this.posts = response;
    this.postsObservable.unsubscribe();
  })
  this.content.scrollToTop();
}
 
 load(value:string){
  this.isSingle=true;
  this.isAll=false;
  this.isPreference=false;

  if(this.currentP !== value && this.until == this.current){
  this.currentP= value;
  this.feedTmp=[];
  this.feedTmp.push(value);

  this.since= this.current - 604800;
  this.until= this.current;

  this.postsObservable = this.facebookService.getFeedItems( this.feedTmp , this.limit, this.since , this. until).subscribe(response => {
    this.posts = response;
    this.postsObservable.unsubscribe();
    })
    this.content.scrollToTop();
 console.log( this.currentP + "/" + this.since +"/" + this.until); 
 this.content.scrollToTop();
   }
  }

addPage( value:string ){
  this.feed.push(value);
  this.storage.set("data", this.feed);
  
  }

scrollUp(){
  this.content.scrollToTop();
}

nextPage ()  {
  setTimeout(() => {
    if(this.isAll){
   this.postsObservable = this.facebookService.getFeedItems(this.feed, this.limit,this.since-=604800 ,this.until-=604800).subscribe(response => {
   this.posts = response;
   this.postsObservable.unsubscribe();
   }) 
  }

    if(this.isPreference){
      this.feedTmp= this.feed.filter( item => item !== "348133635312225" && item !== "highcontrast.ro" &&  item !== "213505205698292");
      this.postsObservable = this.facebookService.getFeedItems(this.feedTmp, this.limit, this.since-=604800 ,this.until-=604800).subscribe(response => {
        this.posts = response;
        this.postsObservable.unsubscribe();
      }) 
    }

      if(this.isSingle){
        this.postsObservable = this.facebookService.getFeedItems(this.feedTmp, this.limit, this.since-=604800 ,this.until-=604800).subscribe(response => {
          this.posts = response;
          this.postsObservable.unsubscribe();
        }) 
      }
  this.scrollUp();
}, 2000);
}

previousPage ()  {
  console.log('Begin async operation'); 
  setTimeout(() => {

  if(this.isAll){
  this.postsObservable = this.facebookService.getFeedItems(this.feed, this.limit, this.since +=604800 ,this.until+=604800).subscribe(response => {
   this.posts = response;
   this.postsObservable.unsubscribe();
  }) 
}

  if(this.isPreference){
    this.feedTmp= this.feed.filter( item => item !== "348133635312225" && item !== "highcontrast.ro" &&  item !== "213505205698292");
    this.postsObservable = this.facebookService.getFeedItems(this.feedTmp, this.limit, this.since+=604800 ,this.until+=604800).subscribe(response => {
      this.posts = response;
      this.postsObservable.unsubscribe();
    }) 
  }


  if(this.isSingle){
    this.postsObservable = this.facebookService.getFeedItems(this.feedTmp, this.limit,    this.since+=604800 ,this.until+=604800).subscribe(response => {
      this.posts = response;
      this.postsObservable.unsubscribe();
    }) 
  }
this.scrollUp();
}, 2000);
}


ngOnInit() {
this.ionScroll = this.myElement.nativeElement.getElementsByClassName('scroll-content')[0];
// On scroll function
this.ionScroll.addEventListener("scroll", () => {
if(this.ionScroll.scrollTop  - this.start > this.threshold) {
this.showheader =true;
this.hideheader = false;
} 
else {
this.showheader =false;
this.hideheader = true;
}
if (this.slideHeaderPrevious >= this.ionScroll.scrollTop - this.start) {
this.showheader =false;
this.hideheader = true;
}
this.slideHeaderPrevious = this.ionScroll.scrollTop - this.start;
});
}
} 