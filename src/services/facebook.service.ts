import { Http } from '@angular/http';  
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';  
import 'rxjs/add/operator/map';

declare var window: any;
declare var FB: any;

@Injectable()
export class FacebookService {
  token: any;


  private API_URL: string = 'https://graph.facebook.com/';
  private ACCESS_TOKEN: string = '1135640869901744|Yf_Mko_X_SUhBVra8W4CBA83xLg';
  


  constructor(private http: Http) { 

// -------------------------Facebook log in----------------------

    function statusChangeCallback(response ) {
    console.log(response);
    if (response.status === 'connected') {
      document.getElementById('status').style.display= 'none';
      testAPI();
    } else {
      if(response.status === 'unknown'){
        document.getElementById('status').style.display= 'block';
        }
    }
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1135640869901744',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.10' // use graph api version 2.8
  });

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
  };

 (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    d.getElementById('reload');
  }(document, 'script', 'facebook-jssdk'));

  function testAPI() {
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);  
    });
  }
   }


  //  --------------------------Get feed----------------

 public getFeedItems(config , limit , since , until) {
    let observable = new Observable(observer => {
     console.log('config: ', config)
          if(config === null) {
          // naspa
        }

        if(config.lenght == 0) {
          // naspa din nou
        }

        var source_ids: string = '';
        config.forEach(item => {
            if(item !== '') {
              if(source_ids !== '') {
                source_ids += ',';
              }
              source_ids += item;
            }
        });

        var url: string = this.API_URL + '?access_token=' + this.ACCESS_TOKEN + '&ids=' + source_ids + '&date_format=U&fields=name,cover,description,feed.since(' + since + ').until(' + until + ').limit(' + limit + '){from,created_time,message,attachments{media},description,link}';

        console.log(url);

        this.facebookGetRequest(url).subscribe(data => {
            var response = data.json();
            var feedItems= [];

            // generate feed array
            Object.keys(response).forEach(key => {
             var tmpGroupInfo: any = [];
             tmpGroupInfo['name'] = response[key].name;
             tmpGroupInfo['id'] = response[key].id;


             try {
             response[key].feed.data.forEach(item => {
                var tmpItem: any = [];
                tmpItem = item;
                tmpItem['source'] = tmpGroupInfo;
                feedItems.push(tmpItem);
                document.getElementById('data').style.display= 'none';
             });
            }

            catch (e){
              console.log((<Error>e).message);
              document.getElementById('data').style.display= 'block';
            }
            });


            // sort by date
            feedItems.sort(function(a, b) {return b.created_time - a.created_time});

            // slice by limit + observer next

            observer.next(feedItems.slice(0, limit));
            
        });
    })
    return observable;
  }

  private facebookGetRequest(url) {
    return this.http.get(url);
          //  Add a comment to this line
  }


  // --------------Get searches---------------------

  getSearched( query:string ): Observable<any[]> {

    this.token = FB.getAuthResponse()['accessToken'];
    let searchquery= this.API_URL + 'search?q=' + query + '&type=page&fields=name,cover,description&access_token=' + this.token ;

    this.facebookGetRequest
    return this.http
        .get(searchquery)
        .map(response => response.json().data);
   }
}



 
  